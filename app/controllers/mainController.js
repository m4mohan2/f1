angular.module('F1FeederApp.mainController', ['chieffancypants.loadingBar', 'ngAnimate'])
        .config(function (cfpLoadingBarProvider) {
            cfpLoadingBarProvider.includeSpinner = true;
        })
        /* Drivers controller */
        .controller('driversController', function ($scope, ergastAPI, $timeout, cfpLoadingBar) {
            /* Drivers controller */
            $scope.nameFilter = null;
            $scope.driversList = [];
            //$scope.loading = true;
            $scope.searchFilter = function (driver) {
                var re = new RegExp($scope.nameFilter, 'i');
                return !$scope.nameFilter || re.test(driver.Driver.givenName) || re.test(driver.Driver.familyName);
            };

            ergastAPI.getDrivers().success(function (response) {
                //$scope.loading = false;
                //Digging into the response to get the relevant data
                $scope.driversList = response.MRData.StandingsTable.StandingsLists[0].DriverStandings;
            });

            $scope.start = function () {
                cfpLoadingBar.start();
            };

            $scope.complete = function () {
                cfpLoadingBar.complete();
            };

            // fake the initial load so first time users can see it right away:
            $scope.start();
            $scope.fakeIntro = true;
            $timeout(function () {
                $scope.complete();
                $scope.fakeIntro = false;
            }, 750);

        })
        /* Driver controller */
        .controller('driverController', function ($scope, $routeParams, ergastAPI) {

            $scope.id = $routeParams.id;
            $scope.races = [];
            $scope.driver = null;

            ergastAPI.getDriverDetails($scope.id).success(function (response) {
                $scope.driver = response.MRData.StandingsTable.StandingsLists[0].DriverStandings[0];
            });

            ergastAPI.getDriverRaces($scope.id).success(function (response) {
                $scope.races = response.MRData.RaceTable.Races;
            });
        })
        /* Races controller */
        .controller('racesController', function ($scope, $http, ergastAPI) {
            $scope.pastRaces = [];
            $scope.racesList = [];
            $scope.filterName = null;

            ergastAPI.getRaceWinners().success(function (response) {
                //Dig into the response to get the relevante data
                $scope.pastRaces = response.MRData.RaceTable.Races;
                ergastAPI.getRaces().success(function (response) {
                    $scope.racesList = response.MRData.RaceTable.Races;
                    angular.forEach($scope.pastRaces, function (race, index) {
                        //Push each winning driver into raceList
                        $scope.racesList[index].Results = race.Results;
                    });
                });
            });
        })
        /* Race controller */
        .controller('raceController', function ($scope, $routeParams, $http, ergastAPI) {
            $scope.id = $routeParams.id;
            $scope.raceResult = [];
            $scope.qualiResult = [];


            ergastAPI.getRaceDetails($scope.id).success(function (response) {
                $scope.race = response.MRData.RaceTable.Races[0];
                $scope.raceResult = response.MRData.RaceTable.Races[0].Results;
            });

            ergastAPI.getQualiDetails($scope.id).success(function (response) {
                $scope.qualiResult = response.MRData.RaceTable.Races[0].QualifyingResults;
            });

            //MOVE THIS SHIT TO A SERVICE
            $http.jsonp('http://ergast.com/api/f1/2013/' + $routeParams.id + '/results.json?callback=JSON_CALLBACK').success(function (response) {
                $scope.race = response.MRData.RaceTable.Races[0];
                $scope.raceResult = response.MRData.RaceTable.Races[0].Results;
            }).error(function (error) {
            });

            //THIS ONE AS WELL< FOR A SEPPARATE SERVICE
            $http.jsonp('http://ergast.com/api/f1/2013/' + $routeParams.id + '/qualifying.json?callback=JSON_CALLBACK').success(function (response) {
                $scope.qualiResult = response.MRData.RaceTable.Races[0].QualifyingResults;
            }).error(function (error) {
            });
        })
        /* menu controller */
        .controller('menuContr', function ($scope, $location) {
            $scope.$on('$routeChangeSuccess', function () {
                $scope.menuActive = $location.path().split("/")[1];
            });
        })
        /*.directive('loading', function () {
            return {
                restrict: 'EA',
                replace: true,
                template: '<div class="loading"><img src="img/ajax-loader.gif"/></div>',
                link: function (scope, element, attr) {
                    scope.$watch('loading', function (val) {
                        if (val)
                            $(element).show();
                        else
                            $(element).hide();
                    });
                }
            }
        })*/;












