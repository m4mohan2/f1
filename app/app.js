angular.module('F1FeederApp', [
  'F1FeederApp.driverService',
  'F1FeederApp.mainController',
  'ngRoute'
]).
config(['$routeProvider', function($routeProvider) {
  $routeProvider.
  	when("/", {templateUrl: "app/views/home.html", controller: "driversController"}).
	when("/home", {templateUrl: "app/views/home.html", controller: "driversController"}).
	when("/driverdetails/:id", {templateUrl: "app/views/driverDetails.html", controller: "driverController"}).
	when("/races/:id", {templateUrl: "app/views/races.html", controller: "raceController"}).

	//otherwise({redirectTo: '/home'});
	otherwise({templateUrl:'app/views/error.html'}); // Render 404 view
}]);